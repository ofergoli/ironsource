const express = require('express');
const _ = require('lodash');
const router = express.Router();
const {fetchTerms, redirectTo} = require('../services/search-service');
const {HTTP_STATUS_CODES: {FAILURE: {NOT_FOUND}, SUCCESS: {OK}}} = require('../config/app-config');

const errHandler = (res) => res.sendStatus(NOT_FOUND);
const resolveResponse = (res, data) => res.status(OK).json(data);

router.get('/:searchTerm', async (req, res) => {
	const {searchTerm} = req.params;
	try {
		return resolveResponse(res, await fetchTerms(searchTerm));
	} catch(err) {
		return errHandler(res);
	}
});

router.get('/redirect/:searchTerm', async (req, res) => {
	const {searchTerm} = req.params;
	return res.redirect(redirectTo(searchTerm));
})

module.exports = router;
