const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const PORT = process.env.PORT || 3060;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use(morgan('combined'));

async function start() {
	try{
		await listen(app);
		await initiateRoutes();
		console.log(`Iron Source App up and running on port ${PORT}`);
	}
	catch (error) {
		console.error('Iron Source App down', error);
	}
}

const initiateRoutes = () => {
	const searchController = require('./controller/search-api');
	app.use('/auto-suggest', searchController);
};

const listen = app => new Promise((resolve, reject) => {
	app.listen(PORT, err => {
		if (err) {
			console.error('Iron Source App down');
			return reject(err);
		}
		return resolve();
	});
});

process.on('uncaughtException', error => {
	console.error('Iron Source: uncaughtException:', error);
	process.exit(1);
});

start();
