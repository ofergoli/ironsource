const axios = require('axios');

module.exports = {
	fetchTerms: async searchTerm => {
		return new Promise(async (resolve, reject) => {
			try {
				const {data: results} = await axios.get(`https://api.bing.com/osjson.aspx?query=${searchTerm}`);
				const [term, termResults] = results;
				return resolve(termResults.map(x => ({value: x, label: x})));
			}
			catch(e) {
				return reject(e);
			}
		})
	},
	redirectTo: searchTerm => `https://www.bing.com/search?q=${searchTerm}`
}
