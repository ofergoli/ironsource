#Iron Source Task

Go to /server and install
```
npm install
```

Run
```
node app.js
```

The server is running on port 3060.

```
http://localhost:3060
```

Load the extention folder into chrome.
It's written with react.js. The react source code is under my-app folder.
also can install the dev env and run with

```
    npm start
    goto
    http://localhost:3000
```

Add the extention to chrome. The auto compelete search will interacts with the api.

License
----

Ofer Golibroda
