import React from 'react';
import './App.css';
import AutoSuggest from './components/auto-suggest2';

function App() {
  return (
    <div className="App">
        <AutoSuggest />
    </div>
  );
}

export default App;
