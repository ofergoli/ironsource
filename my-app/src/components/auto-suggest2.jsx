import React from 'react';
import axios from 'axios';
import Autosuggest from 'react-autosuggest';
import _ from 'lodash';
import './auto-suggest2.css';

const BASE_URL = 'http://localhost:3060/auto-suggest';

function getSuggestionValue(suggestion) {
	return suggestion.value;
}

function renderSuggestion(suggestion) {
	return (
		<span>{suggestion.value}</span>
	);
}

const fetchTerms = (term) => axios.get(`${BASE_URL}/${term}`);

export default class AutoSuggest extends React.Component {
	constructor() {
		super();

		this.state = {
			value: '',
			suggestions: [],
			suggestionValue: ''
		};

		this.debouncedLoadSuggestions = _.debounce(this.loadSuggestions, 300);
	}

	loadSuggestions(value) {
		fetchTerms(value).then(({data}) => {
			this.setState({
				suggestions: data,
			})
		});
	}

	onChange = (event, { newValue }) => {
		this.setState({
			value: newValue
		});
	};

	onSuggestionsFetchRequested = ({ value }) => {
		this.debouncedLoadSuggestions(value);
	};

	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	};

	onSuggestionSelected = (event, {suggestionValue}) => {
		this.setState({
			suggestionValue
		});
	}

	getLink() {
		return `${BASE_URL}/redirect/${this.state.suggestionValue}`;
	}

	render() {
		const { value, suggestions } = this.state;
		const inputProps = {
			placeholder: 'Search Something',
			value,
			onChange: this.onChange
		};

		return (
			<div>
				<h1>Iron Source Search</h1>
				<Autosuggest
					suggestions={suggestions}
					onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
					onSuggestionsClearRequested={this.onSuggestionsClearRequested}
					onSuggestionSelected={this.onSuggestionSelected}
					getSuggestionValue={getSuggestionValue}
					renderSuggestion={renderSuggestion}
					inputProps={inputProps} />
				<a href={this.getLink()}>
					Search
				</a>
			</div>
		);
	}
}
