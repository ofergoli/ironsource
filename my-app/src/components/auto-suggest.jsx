import React, { Component, Fragment } from 'react';
import AsyncSelect from 'react-select/async';
import axios from 'axios';
import classNames from 'classnames';

const BASE_URL = 'http://localhost:3060/auto-suggest';

const fetchTerms = (term) => axios.get(`${BASE_URL}/${term}`);

const filterResults = (results, inputValue) => {
	return results.filter(i =>
		i.label.toLowerCase().includes(inputValue.toLowerCase())
	);
};

const promiseOptions = inputValue =>
	new Promise(async resolve => {
		const {data: results} = await fetchTerms(inputValue);
		resolve(filterResults(results, inputValue));
	});

export default class WithPromises extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedTerm: ''
		}
	}

	getLink() {
		return `${BASE_URL}/redirect/${this.state.selectedTerm}`;
	}

	render() {
		return (
			<Fragment>
				<div class="row">
					<div className="col s4"></div>
					<div className="col s4">
						<h2>Iron Source Task</h2>
					</div>
					<div className="col s4"></div>
				</div>
				<div class="row">
					<div className="col s4"></div>
					<div className="col s4">
						<AsyncSelect cacheOptions
									 defaultOptions
									 loadOptions={promiseOptions}
									 onChange={({value: selectedTerm}) => this.setState({selectedTerm})}/>
					</div>
					<div className="col s1">
						<a href={this.getLink()}
						   style={{marginTop: '8px'}}
						   className={classNames('waves-effect waves-light btn-large', {disabled: this.state.selectedTerm ===	 ''})}>
							Search
						</a>
					</div>
				</div>
			</Fragment>
		);
	}
}
